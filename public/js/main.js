import { Book } from "./modules/book.js";
import { Library } from "./modules/library.js";

let addBookDialog = document.getElementById("book-input");
let library = new Library();
library.loadBooks();

window.userAddBook = function userAddBook() {
	addBookDialog.style.visibility = "visible";
}

window.userCancelBook = function userCancelBook() {
	addBookDialog.style.visibility = "hidden";
}

window.userConfirmBook = function userConfirmBook() {
	let newBook = new Book(
		document.getElementsByName("title")[0].value,
		document.getElementsByName("author")[0].value,
		document.getElementsByName("pages")[0].value,
		document.getElementsByName("read")[0].checked
	);

	library.addBook(newBook);
	
	addBookDialog.reset();
	addBookDialog.style.visibility = "hidden";
}

window.userSetReadStatus = function userSetReadStatus(bookElement) {
	let indexToSet = bookElement.dataset.index;
	if (parseInt(indexToSet) <= library.books.length) {
		library.setReadStatus(indexToSet);
	}
}

window.userDeleteBook = function userDeleteBook(bookElement) {
	let indexToRemove = bookElement.dataset.index;
	if (parseInt(indexToRemove) <= library.books.length) {
		library.removeBook(indexToRemove);
	}
}