export function storageAvailable(type) {
	var storage;
	try {
		storage = window[type];
		var x = '__storage_test__';
		storage.setItem(x, x);
		storage.removeItem(x);
		return true;
	}
	catch(e) {
		return e instanceof DOMException && (
			e.code === 22   ||  // everything except Firefox
			e.code === 1014 ||  // Firefox

			// test name field too, because code might not be present
			e.name === 'QuotaExceededError'         ||  // everything except Firefox
			e.name === 'NS_ERROR_DOM_QUOTA_REACHED'     // Firefox
		) && (
			storage && storage.length !== 0 // acknowledge QuotaExceededError only if there's something already stored
		);
	}
}

export function getObject(key) {
	return JSON.parse(localStorage.getItem(key));
}

export function setObject(key, value) {
	return localStorage.setItem(key, JSON.stringify(value));
}