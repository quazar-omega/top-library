export class Book {
	constructor(title, author, pages, isRead) {

		this.title = title;
		this.author = author;
		this.pages = pages;
		this.isRead = isRead;
		
	}

	info() {
		return `${this.title} by ${this.author}, ${this.pages} pages, ${this.isRead == true ? "read" : "not read yet"}`;
	}

	card(index) {
		let element = document.createElement('div');
		element.className = "book";
		element.dataset.index = `${index}`;
		element.innerHTML = `
			<p class="info">${this.info()}</p>
			<label class="toggle-button">
				<input type="checkbox" ${this.isRead == true ? "checked" : ""} onchange="userSetReadStatus(this.parentElement.parentElement)"/>
				<span class="button">
					<span class="material-icons" class="read"></span>
				</span>
			</label>
			<button class="delete" onclick="userDeleteBook(this.parentNode)">
				<span class="material-icons">delete</span>
			</button>
		`;

		return element;
	}
}