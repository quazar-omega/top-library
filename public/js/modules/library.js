import { Book } from "./book.js";
import * as StorageUtils from "./storage-utils.js";

export class Library {
	constructor() {
		this.elem = document.getElementById("library");
		this.books = [];

		if (StorageUtils.storageAvailable('localStorage')) {
			this.isStorageAvailable = true;
		} else {
			this.isStorageAvailable = false;
		}
		
	}

	showBooks() {
		this.elem.innerHTML = ``;
		for (let i = 0; i < this.books.length; i++) {
			this.elem.appendChild(this.books[i].card(i));
		}
	}

	addBook(book) {
		this.books.push(book);
		this.elem.appendChild(this.books[this.books.length - 1].card(this.books.length - 1));
		this.saveBooks();
	}

	removeBook(index) {
		this.books.splice(index, 1);
		this.showBooks();
		this.saveBooks();
	}

	setReadStatus(index) {
		this.books[index].isRead = !this.books[index].isRead;
		let bookElements = Array.from(document.getElementsByClassName("book"));
		let bookToChange = bookElements.filter(
			(book) => {
				return book.dataset.index == index;
			}
		); 
		
		bookToChange[0].querySelector(".info").innerText = this.books[index].info();

		this.saveBooks();
	}

	loadBooks() {
		if (this.isStorageAvailable == true && localStorage.getItem("books")) {
			let storedBooks = StorageUtils.getObject("books");
			this.books = storedBooks.map(
				(book) => {
					return new Book(
						book.title,
						book.author,
						book.pages,
						book.isRead
					);
				}
			);

			this.showBooks();
		}
	}

	saveBooks() {
		if (this.isStorageAvailable == true) {
			StorageUtils.setObject("books", this.books);
		}
	}
}