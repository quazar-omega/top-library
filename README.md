# [The Odin Project](https://www.theodinproject.com)

## [Full stack Javascript > Library](https://www.theodinproject.com/paths/full-stack-javascript/courses/javascript/lessons/libraryhttps:/)

A simple library website to practice fundamental Javascript concepts

[Click here to open it!](https://quazar-omega.gitlab.io/top-library/)

![](assets/library-screenshot-edited.png)
